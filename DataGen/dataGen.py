__author__ = 'Maciej'

import random
from struct import pack


def normalizeToRange(min, max, value):
    if value > max:
        value = max
    elif value < min:
        value = min
    return value


def generateRecord(length: int) -> tuple:
    record = []
    for i in range(0, normalizeToRange(1, 15, length)):
        record.append(random.uniform(-10000, 10000));
    record.append(float('nan'))
    return tuple(record)


def generateCollection(length: int) -> list:
    if length < 1:
        length = 1

    coll = []
    for i in range(0, length):
        coll.append(generateRecord(random.randint(1, 15)))

    return coll


def printCollection(filename: str, collection: list, serialize: bool = False):
    if serialize:
        with open(filename, 'wb') as file:
            for record in collection:
                for element in record:
                    file.write(pack('d', element))
    else:
        with open(filename, 'w') as file:
            for record in collection:
                file.write(str(record)+'\n')


recordQty = None
filename = None

while recordQty is None:
    try:
        recordQty = int(input('number of records:'))
    except ValueError:
        print("This value is supposed to be an integer.")

filename = input('target file path: ')

db = generateCollection(recordQty)

printCollection(filename, db, True)
printCollection('pt_'+filename, db, False)

print('Done.')

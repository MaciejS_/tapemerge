__author__ = 'Maciej'


from tapemanip.tapereader import TapeReader, TapeEnd
from tapemanip.tapeviewer import TapeViewer
from tapemanip.tapewriter import TapeWriter
from tapesort.func import *
import datagen.dataGen as dataGen
import filecmp
from os import remove
from shutil import copy
from tapesort.sorter import Sorter
from tapesort.merger import Merger
from time import clock

import sys

_failed_tests = []

# PASSED
def dum_detection_test(max: int):
    _result = True
    _layout = [0, 1, 1]
    _add_to = 1

    if Merger.dumDetTEST([0,0,1])['dummies'] != 0:
        return False

    while _result and max(_layout) < fib(max):
        _result = False
        if is_fibonacci(_layout[_add_to]):
            _add_to = (_add_to % 2) + 1     # flip target

        # perform detection
        _dum_data = Merger.dumDetTEST(_layout)

        # verify result
        _verif = list(_layout)
        _verif[_dum_data['tapeID']] += _dum_data['dummies']

        if is_fibonacci(_verif[1]) and is_fibonacci(_verif[2]) and len(set(_verif))==len(_verif):
            print(str(_layout) + '->' + str(_verif) + ' passed')
            _result = True

        _layout[_add_to] += 1

    return _result

# PASSED
def rw_quantiative_test(test_file):
    reader = TapeReader(test_file)
    writer = TapeWriter(test_file+'_cp')

    while not reader.is_tape_over():
        writer.write_record(reader.get_next_record())

    writer.close()

    _rpg, _wpg = reader.get_read_pages_count(), writer.get_written_pages_count()

    print('Read: ', _rpg)
    print('Written: ', _wpg)

    if _rpg == _wpg:
        print("Test passed!")
        remove(test_file+'_cp')
        return True
    else:
        print("Test failed!")
        remove(test_file+'_cp')
        return False

# PASSED
def rw_reliability_test(test_file):
    reader = TapeReader(test_file)
    writer = TapeWriter(test_file+'_cp')

    while not reader.is_tape_over():
        writer.write_record(reader.get_next_record())

    writer.close()

    if filecmp.cmp(test_file, test_file+'_cp'):
        print("Test passed!")
        remove(test_file+'_cp')
        return True
    else:
        print("Test failed")
        remove(test_file+'_cp')
        return False

# PASSED
def data_acquisition_method_independence():
    reader = TapeReader('long.txt')

    with open('pt_long.txt') as file:
        _lines = file.readlines()
        _c = dataGen.generate_from_formatted_string(''.join(_lines))

    tw1, tw2 = TapeWriter('long.txt_cp'), TapeWriter('long.txt_cp2')

    while not reader.is_tape_over():
        tw1.write_record(reader.get_next_record())

    for record in _c:
        tw2.write_record(record)

    tw1.close()
    tw2.close()

    _result = filecmp.cmp('long.txt_cp', 'long.txt_cp2')

    remove('long.txt_cp')
    remove('long.txt_cp2')

    return _result

def generate_new_data_for_sort_test(record_qty: int):
    dataGen.data_generator('mrgtest', record_qty, False, (1, 15), (-10000, 10000))

    copy('./mrgtest.s', './mrgtest.s_ori')
    copy('./mrgtest.txt', './mrgtest.txt_ori')

def generate_non_coalescing_data_set(records: int, record_length: int):
    create_test_files_from_record_list([(records - x,)*record_length for x in range(records)], 'mrgtest')

def generate_fully_coalescing_data_set(records: int, record_length: int):
    create_test_files_from_record_list([(x,)*record_length for x in range(records)], 'mrgtest')


def create_test_files_from_record_list(collection: list, filename: str):
    dataGen.printCollection(filename+'.s', collection, True)
    dataGen.printCollection(filename+'.txt', collection, False)

    copy('./mrgtest.s', './mrgtest.s_ori')
    copy('./mrgtest.txt', './mrgtest.txt_ori')

def merging_logic_test(filename: str, silent: bool):
    # GET INPUT FILE COPY
    _tr = TapeReader(filename)
    _rec_list = []

    try:
        while True:
            _rec_list.append(_tr.get_next_record())
    except TapeEnd:
        pass

    _rec_set = set(_rec_list)

    if len(_rec_list) != len(_rec_set):
        print("Repeating records detected")

    _rec_list.clear()
    _tr.close()

    # SORT
    _s = Sorter()
    _s.sort(filename, Sorter.VERBOSE_STATS_ONLY)

    # VERIFY RESULT
    _tr = TapeReader(filename)
    _result = True
    _r_count = 0

    try:
        while True:
            # CHECK ORDER IN RESULT FILE
            _rec_list.append(_tr.get_next_record())
            if compare_records(_rec_list[-1], _tr.peek_next_record()) < 0:
                _result = False
            _r_count += 1
    except TapeEnd:
        pass



    _rec_set2 = set(_rec_list)
    dataGen.printCollection('result.omg', list(_rec_set))
    print(len(_rec_set), len(_rec_set2), sep=':')

    if _rec_set2 != _rec_set:
        print('DROPPED RECORDS!!')
        return False

    # RESULT WILL BE FALSE IF ANY RECORD WAS LOST
    #_result = _result and _r_count == len(_rec_list)

    return _result and _rec_set2 == _rec_set

def recover_previous_test_case():
    copy('./mrgtest.s_ori', './mrgtest.s')
    copy('./mrgtest.txt_ori', './mrgtest8.txt')

def retain_current_case(ord: int):
    copy('./mrgtest.s_ori', './test_cases/mrgtest{0}.s'.format(ord))
    copy('./mrgtest.txt_ori', './test_cases/mrgtest{0}.txt'.format(ord))

def retrieve_test_case(ord: int):
    copy('./test_cases/mrgtest{0}.s'.format(ord), './mrgtest.s')
    copy('./test_cases/mrgtest{0}.txt'.format(ord), './mrgtest.txt')

def destroy_retained_case(ord: int):
    remove('./test_cases/mrgtest{0}.s'.format(ord))
    remove('./test_cases/mrgtest{0}.txt'.format(ord))

# PASSED
def non_coalescing_data_sort_test():
    for i in range(1, 32):
        for l in range(1, 15):
            #generate_new_data_for_sort_test(i)
            generate_non_coalescing_data_set(i, l)
            retain_current_case(100*i + l)
            if merging_logic_test('mrgtest.s', True):
                destroy_retained_case(100*i + l)
                print('Passed %d-%d' % (i, l))
            else:
                _failed_tests.append(i)
                print('     %d-%d FAILED' % (i, l))

    print(_failed_tests)

# PASSED
def fully_coalescing_data_test():
    for i in range(1, 32):
        for l in range(1, 15):
            generate_fully_coalescing_data_set(i, l)
            retain_current_case(100*i + l)
            if merging_logic_test('mrgtest.s', True):
                destroy_retained_case(100*i + l)
                print('Passed %d-%d' % (i, l))
            else:
                _failed_tests.append(i)
                print('     %d-%d FAILED' % (i, l))

def custom_record_set_merge_test(record_set: list):
    create_test_files_from_record_list(record_set, 'testcase')
    if merging_logic_test('testcase.s', False):
        print('Passed')
    else:
        print('FAILED')

    _tr = TapeReader('testcase.s')
    _tv = TapeViewer(_tr, '')
    _tv.show_all(True)
    _tv.close()
    _tr.close()

def run_tests():
    for i in range(1, 100000, 100):  # record_qty
        for l in range(1, 3):  # test_qty
            generate_new_data_for_sort_test(i)
            retain_current_case(100*i + l)
            if merging_logic_test('mrgtest.s', True):
                destroy_retained_case(100*i + l)
                print('Passed %d-%d' % (i, l))
            else:
                _failed_tests.append(i)
                print('     %d-%d FAILED' % (i, l))

    print(_failed_tests)


def repeat_tests():
    for i in _failed_tests:
        retrieve_test_case(i)
        if merging_logic_test('mrgtest.s', False):
            print('Passed %d' % i)
            destroy_retained_case(i)
        else:
            print('     %d FAILED' % i)

def count_runs(tapefile: str) -> int:
    _tr = TapeReader(tapefile)
    _cnt = 0
    try:
        while True:
            if _tr.get_next_record() > _tr.peek_next_record():
                _cnt += 1
    except TapeEnd:
        _tr.close()

    return _cnt + 1

def REPORT_rand_test():
    for length in [5**x for x in range(1, 10)]:
            start = clock()
            generate_new_data_for_sort_test(length)
            print('Runs: ' + str(count_runs('mrgtest.s')))
            merging_logic_test('mrgtest.s', False)
            print('Time: ' + str(clock() - start) + '\n')

def REPORT_scaling_test():
    for record_siz in [1, 7, 15]:
        print('\n------ RecordSize: %d -------\n' % record_siz)
        for length in [5**x for x in range(1, 10)]:
            start = clock()
            dataGen.data_generator('mrgtest', length, False, (record_siz,)*2, (-10000, 10000))
            print('Runs: ' + str(count_runs('mrgtest.s')))
            merging_logic_test('mrgtest.s', False)
            print('Time: ' + str(clock() - start) + '\n')

def REPORT_noncolaesc_test():
        for record_siz in [7]:
            print('\n------ RecordSize: %d -------\n' % record_siz)
            for length in [5**x for x in range(1, 10)]:
                start = clock()
                generate_non_coalescing_data_set(length, record_siz)
                print('Runs: ' + str(count_runs('mrgtest.s')))
                merging_logic_test('mrgtest.s', False)
                print('Time: ' + str(clock() - start) + '\n')

REPORT_rand_test()

# generate_new_data_for_sort_test(1)
# merging_logic_test('mrgtest.s', True)

#custom_record_set_merge_test([(6,), (5,), (8,), (7,)] + [(2,), (1,), (4,), (3,)] + [(14,), (13,), (16,), (15,)] + [(10,), (9,), (12,), (11,)])

#
# if input('Generate new test? [y/n]') == 'y':
#     generate_new_data_for_sort_test()
# else:
#     recover_previous_test_case()
#
#
# #run_tests()
# #repeat_tests()
#
# records = input("Type records in parentheses, with values separated by colons ")
#
# dataset = dataGen.generate_from_formatted_string(records)
#
# print(dataset)

#print(str(sys.argv))

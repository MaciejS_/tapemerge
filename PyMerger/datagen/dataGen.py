__author__ = 'Maciej'

import random
from struct import pack
import re

def normalizeToRange(min, max, value):
    if value > max:
        value = max
    elif value < min:
        value = min
    return value


def generateRecord(length: int, ints_only: bool=True, a=-10000, b=10000) -> tuple:
    """
    Returned tuple always contains floats.
    If ints_only is true, these floats will have integer values only.
    a and b are minimal and maximal values of a range within whose bounds random values will be generated


    :param length:
    :param ints_only: bool.
    :return: tuple
    """
    if ints_only:
        _record = [random.randint(a, b) for i in range(0, normalizeToRange(1, 15, length))]
    else:
        _record = [random.uniform(a, b) for i in range(0, normalizeToRange(1, 15, length))]
    return tuple(_record)


def generateCollection(length: int, ints_only: bool=False, recordlen_bounds: tuple=(1, 15), recordval_bounds: tuple=(-10, 10)) -> list:
    if length < 0:
        length = 0

    _collection = [generateRecord(random.randint(min(recordlen_bounds), max(recordlen_bounds)),\
                           ints_only, min(recordval_bounds), max(recordval_bounds)) for i in range(0, length)]

    return _collection


def printCollection(filename: str, collection: list, serialize: bool = False):
    if serialize:
        with open(filename, 'wb') as file:
            for record in collection:
                record = record + tuple([float('nan')])
                file.write(pack('d' * len(record), *record))
    else:
        with open(filename, 'w') as file:
            for record in collection:
                file.write(str(record)+'\n')


def generate_from_formatted_string(input_str: str) -> list:
    """
    This function converts formatted data contained in input_str into a processable record collection

    :param input_file: file
    :return: list[tuple] - a list of records


    """
    _record_reg = r"\((.*?)\)"
    _records = re.findall(_record_reg, input_str)
    _collection = []

    for record in _records:
        _values = re.split(', ?', record)
        if _values[-1] == '':       # if record contains only one value, it will be printed as (val,)
            _values.pop()           # yielding a value containing string and an empty string after split
        _collection.append(tuple([float(val) for val in _values[:15]]))

    return _collection


def data_generator(filename: str, record_qty: int, ints_only: bool, recordlen_bounds: tuple, recordval_bounds: tuple):
    _collection = generateCollection(record_qty, ints_only, recordlen_bounds, recordval_bounds)

    printCollection(filename+'.s', _collection, True)
    printCollection(filename+'.txt', _collection, False)



def manual_data_generator():
    recordQty = None
    filename = None

    while recordQty is None:
        try:
            recordQty = int(input('number of records:'))
            ints_only = input('ints only? [y/n] ') == 'y'

        except ValueError:
            print("This value is supposed to be an integer.")

    filename = input('target file path: ')

    db = generateCollection(recordQty, True)

    printCollection(filename, db, True)
    printCollection('pt_'+filename, db, False)

    print('Done.')
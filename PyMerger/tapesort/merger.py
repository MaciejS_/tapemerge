__author__ = 'Maciej'

from tapemanip.tapereader import TapeReader
from tapemanip.tapewriter import TapeWriter
from tapemanip.tapeviewer import TapeViewer
from tapemanip.tape import TapeEnd
from tapesort.func import *

import shutil
import os

class Merger:
    """
    #:type __tape_files = list[str]
    """

    __writer = None
    __readers = [None, None]

    #   _tape_state structure:
    #   index: the same as index of tape file in tapes tuple
    #   value[0]: ROLE value of this tape in current phase : int
    #           reader roles have values -1 and 1
    #           writer role has value 0
    #   value[1]: number of RUNS stored on this tape : int
    __tape_state = None
    __tapes = None

    # __STAT_counters is a dict containing information about number of read and written pages as well as number of
    #                           performed merging phases
    __STAT_counters = None

    __WRITER_ROLE = 0
    __READER_ROLE = 1

    def __init__(self, tapes: tuple, tape_state: list, silent: bool):
        """
        :param tapes: tuple containing names of tape files to be used during merge process
        :param tape_state: list containing information about run quantity and assigned tape role
        :param silent: if true, tape state won't be displayed
        :return:
        """
        self.__prepare_tape_manipulators(tapes, tape_state)
        self.__tape_state = ([self.__WRITER_ROLE, tape_state[0]], [self.__READER_ROLE, tape_state[1]], [-self.__READER_ROLE, tape_state[2]])
        self.__tapes = tapes
        # assert that tape marked for writing is empty
        assert self.__tape_state[self.__WRITER_ROLE][1] == 0

        self.__STAT_counters = {'phases': 0,
                                'reads': 0,
                                'writes': 0}

        self.__silent = silent

    def polyphase_merge(self):
        """
        tapes is a tuple containing paths to tape files, which must be opened by either TapeReader or TapeWriter
        tape_state is a list returned by sorter.__distribute_records, it contains information about number of runs stored on each of tapes

        :param tapes: tuple
        :param tape_state: list
        :return:
        """

        if self.__sum_runs() == 0:
            print('Source tape is empty!')
            return  # if original tape is empty, quit

        # merge dummies             # extract fields containing numbers of runs from __tape_state
        _dum_data = Merger.__detect_dummies([state[1] for state in self.__tape_state])
        self.__merge_dummies(_dum_data)

        while self.__sum_runs() > 1:
            if not self.__silent:
                print("Phase %d" % (self.__STAT_counters['phases']))
                for reader in self.__readers:
                    _viewer = TapeViewer(reader, reader.get_tape_name())
                    _viewer.show_all(True)
                    _viewer.close()
            self.__single_phase(self.__tapes, self.__tape_state)
            self.__STAT_counters['phases'] += 1

        self.__cleanup()

    def get_stats(self) -> dict:
        """
        Returns number of merging phases as well as number of tape r/w operations
        :return: dict{str: int} -> keys('phases', 'reads', 'writes')
        """
        return self.__STAT_counters


    def __prepare_tape_manipulators(self, tapes: tuple, tape_state: list):
        # convert tape names tuple to list and remove name associated with writer
        __read_tapes = list(tapes)
        __read_tapes.pop(tape_state.index(self.__WRITER_ROLE))

        self.__writer = TapeWriter(tapes[tape_state.index(self.__WRITER_ROLE)])

        # create two tapereaders using a list of names of tapes marked for reading
        self.__readers = [TapeReader(tape) for tape in __read_tapes]


    def __single_phase(self, tapes: tuple, tape_state: tuple):

        _emptied_tapes = []
        _records = [None, None]

        _runs = 0

        while not _emptied_tapes:
            try:
                # only peek records
                _records[0] = self.__readers[0].peek_next_record()
                _records[1] = self.__readers[1].peek_next_record()
                _single_merge_complete = False

                # merge two corresponding runs
                while not _single_merge_complete:
                    # select the tape containing a lesser record
                    _selected_reader = adjust_to_range((0, 1), compare_records(_records[1], _records[0]))

                    # _prev_record will be written to tape, so it can be safely removed from buffer
                    _prev_record = self.__readers[_selected_reader].get_next_record()
                    self.__writer.write_record(_prev_record)

                    try:
                        _records[_selected_reader] = self.__readers[_selected_reader].peek_next_record()
                    except TapeEnd as exc:
                        _emptied_tapes.append(str(exc))    # if one of the tapes ends at this point, we still need to merge the remainder of run from the other tape

                    # check whether a merged run on either of tapes has ended (tape end implies a finished run)
                    if compare_records(_prev_record, _records[_selected_reader]) < 0 or _emptied_tapes:
                        # run over at tapeReader0
                        if _selected_reader == 0:
                            self.__dump_remainder_of_run(self.__readers[1])
                        elif _selected_reader == 1:
                            self.__dump_remainder_of_run(self.__readers[0])
                        else:
                            raise HackFailException("Somehow, a non-existing reader had been selected")
                        _runs += 1
                        _single_merge_complete = True

            except TapeEnd as exc:
                _emptied_tapes.append(str(exc))
                _runs += 1 # TapeEnd is raised when last record had been read from tape, this implies a completed run merge

        self.__update_run_numbers(_runs, _emptied_tapes)
        self.__reassign_roles(_emptied_tapes)

        # a part of a run is left in buffer when other tape ends and next phase begins


    def __dump_remainder_of_run(self, source_reader: TapeReader):
        """
        This method writes all records from current run remaining on source_reader tape to self.__writer
        :param source_reader:
        :return:
        """
        _run_dumped = False

        while not _run_dumped:
            _record = source_reader.get_next_record()
            self.__writer.write_record(_record)
            _run_dumped = compare_records(_record, source_reader.peek_next_record()) < 0

    @classmethod
    def dumDetTEST(cls, tape_state: list) -> dict:
        return cls.__detect_dummies(tape_state)

    @staticmethod
    def __detect_dummies(tape_state: list) -> dict:
        """
        This function determines on which tape should dummy records be placed
        Returned object is a following dict: {'tapeID' : int, 'dummies' : int}

        :param tape_state: list[int]
        :return: dict{'tapeID' : int, 'dummies' : int}
        """
        _dum_holding_tape = None
        _maxval = max(tape_state)

        # find a tape holding a non-fibonacci number of runs, this tape will contain dummies
        for run_count in tape_state[1:]:
            if not is_fibonacci(run_count) and run_count > 0:
                _dum_holding_tape = tape_state.index(run_count)

        if _dum_holding_tape is not None:
            return {'tapeID': _dum_holding_tape, 'dummies': next_fib(_maxval+1) - tape_state[_dum_holding_tape]}
        else:
            if not is_fibonacci(_maxval):
                raise ValueError('Both tapes hold non-fibonacci number of runs, distribution phase failure')
            else:
                if tape_state.count(_maxval) > 1 and _maxval > 1:
                    # if both tapes contain the same number of records and both are fibonacci numbers
                    # _maxval+1 forces next_fib to return next value (this behaviour is intended)
                    return {'tapeID': tape_state.index(_maxval), 'dummies': next_fib(_maxval+1) - _maxval}
                else:
                    return {'tapeID': tape_state.index(_maxval), 'dummies': next_fib(_maxval) - _maxval}



    def __update_run_numbers(self, runs_merged: int, emptied_tapes_names: list):
        if len(emptied_tapes_names) == 1:   # typical situation, other reader still holds some records
            other_reader_role_id = -self.__get_tape_role(emptied_tapes_names[0]) # HACK: works because tape reader role IDs are -1 and 1

            self.__tape_state[self.__get_tape_id(self.__get_tape_name(other_reader_role_id))][1] -= runs_merged

        self.__tape_state[self.__get_tape_id(self.__get_tape_name(self.__WRITER_ROLE))][1] += runs_merged
        for emptied_tape_name in emptied_tapes_names:
            self.__set_runs_number(emptied_tape_name, 0)


    def __reassign_roles(self, finished_tapes_names: list):
        self.__writer.close()
        self.__STAT_counters['writes'] += self.__writer.get_written_pages_count()

        # role reassignment occurs only if previous phase was not the last one
        if len(finished_tapes_names) == 1:
            finished_tape_name = finished_tapes_names[0]

            _finished_tape_reader_id = self.__get_reader_id(finished_tape_name)

            self.__readers[_finished_tape_reader_id].close()    # close reader with emptied tape
            self.__STAT_counters['reads'] += self.__readers[_finished_tape_reader_id].get_read_pages_count()
            self.__writer = TapeWriter(finished_tape_name)      # open emptied tape for writing
            self.__readers[_finished_tape_reader_id] = TapeReader(self.__get_tape_name(self.__WRITER_ROLE))  # open previous writer tape for reading

            # writer and finished reader swap roles
            self.__set_role_value(self.__get_tape_name(self.__WRITER_ROLE), self.__get_tape_role(finished_tape_name))
            self.__set_role_value(finished_tape_name, self.__WRITER_ROLE)
        else:
            self.__STAT_counters['reads'] += sum([reader.get_read_pages_count() for reader in self.__readers])






    def __merge_dummies(self, dum_data: dict):
        """
        This method is meant to be called only once at the very beginning of merging process, that's why it uses __writer and __readers objects without any sanity-checks
        dum_data is a dict as returned by Merger.__detect_dummies

        :param dum_data: dict
        :return:
        """

        # tapeID may take values 1 or 2, (tapeID % 2 +1) will return 1 if tapeID == 2 and 2 if tapeID == 1
        _source_reader_id = self.__get_reader_id(self.__tapes[(dum_data['tapeID'] % 2) + 1])
        _run_count = 0


        while _run_count < dum_data['dummies']:
            _prev_record = self.__readers[_source_reader_id].get_next_record()
            self.__writer.write_record(_prev_record)
            if compare_records(_prev_record, self.__readers[_source_reader_id].peek_next_record()) == -1:
                _run_count += 1

        # increment run counter on writer tape
        self.__tape_state[self.__get_tape_id(self.__get_tape_name(self.__WRITER_ROLE))][1] = _run_count
        self.__tape_state[(dum_data['tapeID'] % 2) + 1][1] -= _run_count



    def __get_tape_id(self, tape_name: str) -> int:
        return self.__tapes.index(tape_name)

    def __get_reader_id(self, tape_name: str) -> int:
        for reader in self.__readers:
            if reader.get_tape_name() == tape_name:
                return self.__readers.index(reader)

    def __get_tape_role(self, tape_name: str) -> int:
        return self.__tape_state[self.__tapes.index(tape_name)][0]

    def __get_tape_name(self, tape_role: int) -> str:
        for entry in self.__tape_state:
            if entry[0] == tape_role:
                return self.__tapes[self.__tape_state.index(entry)]

    def __set_runs_number(self, tape_name: str, record_number: int):
        self.__tape_state[self.__tapes.index(tape_name)][1] = record_number

    def __set_role_value(self, tape_name: str, role_value: int):
        self.__tape_state[self.__tapes.index(tape_name)][0] = role_value

    def __sum_runs(self):
        _sum = 0
        for entry in self.__tape_state:
            _sum += entry[1]
        return _sum

    def __cleanup(self):
        """
        Closes all tape manipulators and removes temporary files
        :return:
        """
        self.__writer.close()

        for reader in self.__readers:
            reader.close()

        # if data ended up on one of distribution tapes, move it to original tape
        if self.__get_tape_name(self.__WRITER_ROLE) != self.__tapes[0]:
            shutil.copy(self.__get_tape_name(self.__WRITER_ROLE), self.__tapes[0])

        os.remove('tmp1.tape')
        os.remove('tmp2.tape')

class HackFailException(Exception):

    def __init__(self, reason: str):
        self.__reason = reason

    def __str__(self):
        return repr(self.__reason)


__author__ = 'Maciej'

from tapemanip.tapereader import TapeReader, TapeEnd
from tapemanip.tapewriter import TapeWriter
from tapemanip.tapeviewer import TapeViewer
from tapesort.func import compare_records
from tapesort.merger import Merger

import tapesort.func as func

class Sorter:
    """
    :type __tape_files = list[str]
    """

    __tape_files = None

    VERBOSE_HIGH = 3
    VERBOSE_NORMAL = 2
    VERBOSE_STATS_ONLY = 1
    VERBOSE_SILENT = 0

    def __init__(self):
        pass

    def sort(self, tape_file_name: str, verbose_level: int):
        """
        This method sorts records on given tape.
        Verbose levels:
            SILENT - do not print tape status and sort statistics
            NORMAL - print tape state at beginning and end of sorting process, also print sorting stats
            HIGH - NORMAL + print run-holding tapes status after every phase

        :param tape_file_name: str
        :param verbose_level: int
        :return:
        """

        self.__tape_files = (tape_file_name, 'tmp1.tape', 'tmp2.tape')

        _silent = False if verbose_level == Sorter.VERBOSE_HIGH else True

        if verbose_level > Sorter.VERBOSE_STATS_ONLY:
            print("Initial tape state:")
            _tv = TapeViewer(TapeReader(tape_file_name), tape_file_name)
            _tv.show_all(True)
            _tv.close()

        try:
            _distribution_info = self.__distribute_records()
        except TapeEnd:     # if first read fails, TapeEnd will fall through and be caught here
            print('Source tape is empty!')
            return

        if sum(_distribution_info) == 1:
            return

        _merger = Merger(self.__tape_files, _distribution_info, _silent)

        _merger.polyphase_merge()

        _stats = _merger.get_stats()

        if _stats['phases'] == 0:
            print('Tape was already sorted!')

        if verbose_level > Sorter.VERBOSE_STATS_ONLY:
            print("Final tape state:")
            _tv = TapeViewer(TapeReader(tape_file_name), tape_file_name)
            _tv.show_all(True)

        if verbose_level > Sorter.VERBOSE_SILENT:
            print("Stats:")
            print('Phases: %d' % _stats['phases'])
            print('Read pages: %d' % _stats['reads'])
            print('Written pages: %d' % _stats['writes'])


    def __distribute_records(self) -> list:
        """
        This private method distributes records from source tape onto two additional tapes
        Return value is a tuple containing distribution information that allows to determine location of dummy runs

        :return: list[int]
        """
        _reader = TapeReader(self.__tape_files[0])
        _writers = [TapeWriter(self.__tape_files[1]), TapeWriter(self.__tape_files[2])]
        _written_runs = [0, 0]
        _active_writer = 0
        _fib_index, _fib_val = 1, 1

        # on-tape coalescing detection
        _tape_tail = [None, None]
        _coalescing_to_prev_run = False

        _tape_over = False

        _prev = _reader.get_next_record()

        while not _tape_over:
            try:
                _writers[_active_writer].write_record(_prev)
                _tape_tail[_active_writer] = _prev
                _record = _reader.get_next_record()

                # if coalescing won't occur
                if not Sorter.coalesces(_prev, _record):
                    if not _coalescing_to_prev_run:
                        _written_runs[_active_writer] += 1

                    # on-tape coalescing to previous run applies only to the first run after tape switch
                    # this variable must be unset at this point to ensure proper run counting
                    _coalescing_to_prev_run = False

                    # if it's time to change the active tape
                    if _written_runs[_active_writer] == _fib_val:
                        _fib_index += 1
                        _active_writer = (_active_writer + 1) % 2    # switch active writer
                        _fib_val = func.fib(_fib_index)
                        _coalescing_to_prev_run = Sorter.coalesces(_tape_tail[_active_writer], _record)


                _prev = _record

            except TapeEnd:
                _tape_over = True
                if not _coalescing_to_prev_run:
                    _written_runs[_active_writer] += 1

        for writer in _writers:
            writer.close()

        return [0]+_written_runs

    @staticmethod
    def coalesces(record1: tuple, record2: tuple):
        """
        Returns True if record2 will coalesce with record1
        Records don't coalesce to Nones

        :param record1:
        :param record2:
        :return:
        """
        return record1 is not None and compare_records(record1, record2) == 1

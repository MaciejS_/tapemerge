__author__ = 'Maciej'

from math import log, ceil


def adjust_to_range(rng: range, val: int) -> int:
    if val < min(rng):
        return min(rng)
    elif val > max(rng):
        return max(rng)
    else:
        return val


def compare_records(record1: tuple, record2: tuple):
    """
    if record1 > record2 then -1 is returned
    if record1 == record2 then 0 is returned
    if record1 < record2 then 1 is returned


    :param record1: tuple
    :param record2: tuple
    :return: int
    """

    # handle Nones
    if record1 is None:
        return -1 if record2 is not None else 0
    elif record2 is None:
        return 1 if record2 is not None else 0

    x = set(record1)
    y = set(record2)

    intersect = x & y

    x -= intersect
    y -= intersect

    if x == y:
        return 0
    elif len(x) == 0:
        return 1
    elif len(y) == 0:
        return -1
    else:
        if max(x) > max(y):
            return -1
        elif max(x) < max(y):
            return 1


def fib(n: int) -> int:
    """
    Returns n-th element of Fibonacci's sequence (starting from 0)
    :param n: int
    :return: int
    """

    a, b = 1, 1
    if n < 1:
        return 1
    else:
        for i in range(1, n):
            a, b = b, a+b

    return a


def next_fib(fib_val: int) -> int:
    """
    Returns next value in fibonacci's sequence
    For a value from fibonacci sequence, the same value is returned

    :param fib_val: int
    :return: int
    """

    # prevent illegal log base
    if fib_val < 1:
        return 0

    # a bit of a hack here, it under-approximates seq. index of fib_val in order to cut time wasted on loop below
    i = ceil(log(fib_val, 1.65))
    while fib(i) < fib_val:
        i += 1

    return fib(i)


def is_fibonacci(value: int):
    if value < 1:
        return False

    return next_fib(value) == value


__author__ = 'Maciej'

from struct import unpack
from math import isnan

from tapemanip.tape import Tape, TapeEnd


class TapeReader:
    """
    :type __tape : Tape
    :type __tape_file : str
    :type __BUFFER_SIZE : int

    :type __records : list[tuple]

    :type __end_of_tape_flag : bool
    :type __split_record: tuple
    """

    __tape = None
    __tape_file = None
    __BUFFER_SIZE = 512

    __records = []

    __end_of_tape_flag = False
    __split_record = None

    def __init__(self, tape_file_path: str):
        self.__tape = Tape(tape_file_path, Tape.READ_MODE)
        self.__tape_file = tape_file_path

    def get_more_records(self) -> list:
        if self.__end_of_tape_flag:
            raise TapeEnd(self.__tape_file)
        else:
            self.__extract_records(self.__tape)
            return self.__records

    def get_next_record(self) -> tuple:
        while not self.__records:   # while loop is needed in when records are longer that a page
            self.__extract_records(self.__tape)
        return self.__records.pop(0)

    def peek_next_record(self) -> tuple:
        """
        Returns value of first record without removing it from the buffer
        :return: tuple
        """
        while not self.__records:
            self.__extract_records(self.__tape)
        return self.__records[0]

    def remove_first_record(self):
        """
        Removes first record from the buffer
        """
        if not self.__records:
            raise TapeEnd(self.__tape_file)
        else:
            self.__records.pop(0)


    def is_tape_over(self):
        return self.__end_of_tape_flag and not len(self.__records)


    def close(self):
        self.__tape.close()

    def get_read_pages_count(self) -> int:
        """
        Returns number of pages read from tape
        :return: int
        """
        return self.__tape.get_IO_operation_count()

    def get_tape_name(self) -> str:
        return self.__tape_file

    def __deepcopy__(self, memo):
        from copy import deepcopy
        cls = self.__class__
        result = cls.__new__(cls)
        result.__records = deepcopy(self.__records)
        result.__split_record = deepcopy(self.__split_record)
        result.__tape = deepcopy(self.__tape)
        result.__tape_file = self.__tape_file
        return result

    def _seek_pages(self, page_count: int):
        """
        Method required by TapeViewer, not meant to be used elsewhere
        :param page_count:
        :return:
        """
        self.__tape.seek(page_count * TapeReader.__BUFFER_SIZE)

    def __load_page(self, tape: Tape) -> bytes:
        # since a single page is retained in buffer, TapeEnd is raised when TapeReader buffer had been emptied as well
        if self.__end_of_tape_flag:
            raise TapeEnd(self.__tape_file)

        _buffer = tape.read(TapeReader.__BUFFER_SIZE)

        # set end_of_tape flag if _buffer isn't full
        self.__end_of_tape_flag = len(_buffer) < TapeReader.__BUFFER_SIZE

        if not _buffer:
            raise TapeEnd(self.__tape_file)

        return _buffer

    def __extract_records(self, tape: Tape):
        """
        This method reads up to a page (size determined by __BUFFER_SIZE) worth of records and stores it in self.__records
        :param tape:
        :return: None
        """

        _raw_data = self.__load_page(tape)
        format_string = str(len(_raw_data) // 8) + 'd'
        values = unpack(format_string, _raw_data)

        self.__records = []
        start = 0
        end = 0

        # merge split record
        if self.__split_record is not None:
            nanpos = 0
            while not isnan(values[nanpos]):
                nanpos += 1

            self.__records = [self.__split_record + values[:nanpos]]
            values = values[nanpos + 1:]
            self.__split_record = None

        # it may happen, that split delimiter will be the only value in buffer
        if values:
            # reassemble values into records
            for value in values:
                end += 1
                if isnan(value):
                    self.__records.append(values[start:end-1])
                    start = end

            # check for split record at the end of buffer
            if isnan(values[-1]) is False:
                self.__split_record = values[start:end]
            else:
                self.__split_record = None

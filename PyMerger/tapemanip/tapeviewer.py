__author__ = 'Maciej'

from copy import deepcopy
import sys

from tapemanip.tapereader import TapeReader
from tapesort.func import compare_records
from tapemanip.tape import TapeEnd


class TapeViewer():
    """
    :type __tape_reader: TapeReader
    :type __displayed_tape_name: str
    """

    __tape_empty = False

    __tape_reader = None
    __displayed_tape_name = None

    def __init__(self, tape_reader: TapeReader, displayed_tape_name: str):
        self.__tape_reader = deepcopy(tape_reader)
        # give me a break
        self.__tape_reader._seek_pages(self.__tape_reader.get_read_pages_count())
        self.__displayed_tape_name = displayed_tape_name

    def show_all(self, group_into_runs: bool):
        self.print_all(sys.stdout, group_into_runs)

    def print_all(self, target_file, group_into_runs: bool):
        print('Tape ', self.__displayed_tape_name)
        if self.__tape_empty:
            print('<empty>')
            return

        _print_tape_empty_msg = True

        if group_into_runs:
            while not self.__tape_empty:
                try:
                    target_file.write(self.__print_next_run_to_string() + '\n')
                    _print_tape_empty_msg = False
                except TapeEnd:
                    if _print_tape_empty_msg:
                        target_file.write('<empty>\n')
                    self.__tape_empty = True
        else:
            while not self.__tape_empty:
                try:
                    target_file.write(str(self.__tape_reader.get_next_record()) + '\n')
                    _print_tape_empty_msg = False
                except TapeEnd:
                    if _print_tape_empty_msg:
                        target_file.write('<empty>\n')
                    self.__tape_empty = True

        target_file.write('\n')

    def close(self):
        """
        Closes the tape
        """
        self.__tape_reader.close()

    def __print_next_run_to_string(self) -> str:
        _run = [self.__tape_reader.get_next_record()]
        try:
            _record = self.__tape_reader.peek_next_record()

            while compare_records(_run[-1], _record) >= 0 and not self.__tape_empty:
                _run.append(_record)
                self.__tape_reader.remove_first_record()
                try:
                    _record = self.__tape_reader.peek_next_record()
                except TapeEnd:
                    self.__tape_empty = True
        except TapeEnd:
            self.__tape_empty = True

        return str(_run)
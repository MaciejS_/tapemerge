__author__ = 'Maciej'


class Tape:
    """
    :type __tape_file : file
    :type __STAT_read_count : int
    :type __STAT_write_count : int
    """
    __tape_file = None

    READ_MODE = 'rb'
    WRITE_MODE = 'wb'

    __STAT_read_count = 0
    __STAT_write_count = 0

    def __init__(self, tape_file_path: str, mode: str):
        self.open(tape_file_path, mode)
        self.__selected_mode = mode

    def __del__(self):
        self.close()

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        result.__STAT_read_count = self.__STAT_read_count
        result.__STAT_write_count = self.__STAT_write_count
        result.__tape_file = open(self.__tape_file.name, self.__get_mode())
        result.__selected_mode = self.__selected_mode
        return result

    def open(self, tape_file_path: str, mode: str):
        self.__tape_file = open(tape_file_path, mode)
        self.__STAT_read_count = 0
        self.__STAT_write_count = 0

    def close(self):
        if self.is_open():
            self.__tape_file.close()
            self.__tape_file = None

    def is_open(self) -> bool:
        return self.__tape_file is not None

    def read(self, byteCount: int) -> bytes:
        """
        Returned data is a string of bytes, which must be unpacked with struct.unpack
        :param byteCount: int
        :return: list[int]
        """
        self.__STAT_read_count += 1
        return self.__tape_file.read(byteCount)

    def write(self, data: str):
        """
        Data must be a string of bytes returned by struct.pack
        :param data:
        :return:
        """
        self.__tape_file.write(data)
        self.__STAT_write_count += 1

    def get_IO_operation_count(self) -> int:
        """
        Depending on mode chosen upon construction of this object, either number of read or written pages will be returned
        :return: int
        """
        if self.__selected_mode == Tape.READ_MODE:
            return self.__STAT_read_count
        elif self.__selected_mode == Tape.WRITE_MODE:
            return self.__STAT_write_count

    def __get_mode(self) -> str:
        """
        Returns Tape.READ_MODE if this tape is open for reading or Tape.WRITE_MODE if this tape is open for writing
        :return: str
        """
        return Tape.READ_MODE if self.__tape_file.readable() else Tape.WRITE_MODE

    def seek(self, offset: int):
        self.__tape_file.seek(offset)

class TapeEnd(Exception):

    def __init__(self, tape_file_name: str):
        self.__tape_file_name = tape_file_name

    def __str__(self):
        return self.__tape_file_name

    pass
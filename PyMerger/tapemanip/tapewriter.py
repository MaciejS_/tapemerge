__author__ = 'Maciej'

from struct import pack

from tapemanip.tape import Tape


class TapeWriter():
    """
    :type __tape: tape
    :type __write_buffer : bytestr
    """
    __tape = None
    __BUFFER_SIZE = 512

    __write_buffer = b''

    def __init__(self, tape_file_path: str):
        self.__tape = Tape(tape_file_path, Tape.WRITE_MODE)

    def __del__(self):
        self.close()

    def write_run(self, run: list):
        for record in run:
            self.write_record(record)

    def write_record(self, record: tuple):
        # serialize record and append terminator
        bytestr = pack('d' * len(record), *record) + pack('d', float('nan'))
        self.__write(bytestr)

    def close(self):
        if self.__tape.is_open():
            self.__tape.write(self.__write_buffer)  # flush buffer
            self.__tape.close()


    def get_written_pages_count(self) -> int:
        """
        Returns number of performed page writes
        :return: int
        """
        return self.__tape.get_IO_operation_count()

    def __write(self, record: str):
        buffer_space = self.__BUFFER_SIZE - len(self.__write_buffer)

        # if record won't fit in write buffer
        if len(record) > buffer_space:
            # complement buffer and write to tape
            self.__tape.write(self.__write_buffer + record[:buffer_space])
            self.__write_buffer = b''

            # recursively call write on remainder of bytestring
            self.__write(record[buffer_space:])
        else:
            self.__write_buffer += record

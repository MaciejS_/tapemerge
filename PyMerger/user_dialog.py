__author__ = 'Maciej'

import datagen.dataGen as dataGen
from tapesort.sorter import Sorter
from ast import literal_eval as make_tuple


def user_dialog() -> str:
    """
    Asks user a set of questions
    A dataset will be created in current directory as a result.


    :return: None
    """

    print("TapeSort v1.0")
    print("Polyphase Edition", end='\n\n')

    choice = ' '
    records = []
    filename = 'dataset.s'

    while choice.lower() not in 'kr' or len(choice) > 1:
        choice = input("Select data source: [K]eyboard or [R]andom generator: ")

    if choice.lower() == 'k':
         while len(records) < 1:
            print("Type records in parentheses, with values separated by commas.")
            input_str = input('> ')
            try:
                records = dataGen.generate_from_formatted_string(input_str)
            except ValueError:
                records = []

            if len(records) == 0:
                print("Incomprehensible input, try again")


    else:
        _len = ''
        _ints_only = ' '
        _val_bounds = ' '

        while not _len.isdecimal():
            _len = input("Number of records to generate: ")
        while _ints_only.lower() not in 'yn' or len(_ints_only) > 1:
            _ints_only = input("Generate only integer values? [y/n]: ")
        while _val_bounds.lower() not in 'yn' or len(_val_bounds) > 1:
            _val_bounds = input("Do you wish to specify value boundaries? [y/n]: ")

        if _val_bounds == 'y':
            _val_bounds = make_tuple(input('Specify bounds in parentheses (min, max): '))
        else:
            _val_bounds = None

        _ints_only = True if _ints_only == 'y' else False

        if isinstance(_val_bounds, tuple):
            records = dataGen.generateCollection(int(_len), _ints_only, recordval_bounds=_val_bounds)
        else:
            records = dataGen.generateCollection(int(_len), _ints_only)

    dataGen.printCollection(filename, records, True)

    return filename


def prompt_verbose_level():
    choice = ' '
    while choice.lower() not in 'hns' or len(choice) > 1:
        choice = input("Select verbose level: [H]igh, [N]ormal, [S]ilent: ")

    if choice.lower() == 'h':
        return Sorter.VERBOSE_HIGH
    elif choice.lower() == 'n':
        return Sorter.VERBOSE_NORMAL
    else:
        return Sorter.VERBOSE_SILENT

def help_msg():
    print('PyMerge v1.0', end='\n\n')
    print('Options:')
    print('\t-h - displays this message')
    print('\t-f [file] - reads input data from file')
    print('\t-g [recordqty, file] - generates a random data set containing a given number of records in the specified file')
    print('\t-c [file] - converts data from stdin to packed binary format and stores in given file')
    print('\t-s - silent mode')
    print('\t-v - verbose mode')
    print('\t-x - only stats will be displayed')

    print('\nIf you wish to store messages in a file, use output redirection')


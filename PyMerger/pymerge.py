__author__ = 'Maciej'

from user_dialog import user_dialog, prompt_verbose_level, help_msg
from tapemanip.tapeviewer import TapeViewer
from tapemanip.tapereader import TapeReader
from tapesort.sorter import Sorter
from datagen.dataGen import *
import sys
import os


def run():
    args = sys.argv[1:]

    _user_interaction = True
    _quit = False
    _verbose = Sorter.VERBOSE_NORMAL
    _filename = None

    _verbose_opts = set(['-s', '-x', '-v'])

    print(args)

    if len(args) > 0:
        _chosen_verbose = list(_verbose_opts.intersection(set(args)))
        if '-g' in args:
            if len(args) > 1:
                _path = None
                _qty = None
                for arg in args:
                    if arg.isalpha() or '.' in arg:
                        _path = arg
                    if arg.isdigit():
                        _qty = int(arg)
                if _path is not None and _qty is not None:
                    data_generator(_path, _qty, True, (1, 15), (-10000, 10000))
                if _path is None or _qty is None:
                    print('Missing arguments')
            else:
                 print('Missing arguments')
            _quit = True
        if '-c' in args and not _quit:
            if len(args) > 1:
                for arg in args:
                    if arg.isalpha():
                        printCollection(arg, generate_from_formatted_string(input()), True)
                        _filename = arg
                        break
                if _filename is None:
                    print('-c chosen and file not specified, aborting')
            else:
                print('-c chosen and file not specified, aborting')
            _quit = True
        if '-f' in args and not _quit:
            if len(args) > 1:
                for arg in args:
                    if '.' in arg and os.path.isfile(arg):
                        _filename = arg
                        _user_interaction = False
                        break
                if _filename is None:
                    print('-f chosen and file not specified, aborting')
                    _quit = True

            else:
                print('-f chosen and file not specified, aborting')
                _quit = True
        if len(_chosen_verbose) == 1:
            _chosen_verbose = _chosen_verbose[0]
            if _chosen_verbose == '-s':
                _verbose = Sorter.VERBOSE_SILENT
            if _chosen_verbose == '-x':
                _verbose = Sorter.VERBOSE_STATS_ONLY
            if _chosen_verbose == '-v':
                _verbose = Sorter.VERBOSE_HIGH
        elif len(_chosen_verbose) > 1:
            print('Multiple verbose options chosen, aborting...')
            _quit = True
        if '-h' in args and len(args) == 1:
            help_msg()
            _quit = True

    if not _quit:
        _sorter = Sorter()
        if _user_interaction:
            _filename = user_dialog()
            if _verbose is None:
                _verbose = prompt_verbose_level()

        _sorter.sort(_filename, _verbose)

run()
